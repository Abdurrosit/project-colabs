@extends('layouts.master')

@section('judul')
Halaman Detail Kategori Buku
@endsection


@section('content')



<h1>{{$kategori_buku->kategori}}</h1>
<p>{{$kategori_buku->keterangan}}</p>

<a href="/kategori_buku" class="btn btn-secondary">Kembali</a>


@endsection