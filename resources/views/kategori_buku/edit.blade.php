@extends('layouts.master')

@section('judul')
Halaman Edit Kategori Buku
@endsection
@section('content')
<div class="row">
    <div class="card">
        <form action="/kategori_buku/{{$kategori_buku->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method ('PUT')
            <div class="body">
            <h3 class="card-aside-title ">Tambah Data Kategori Buku</h3>
                <div class="row ">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Kategori</label>
                                <input type="text" value="{{$kategori_buku->kategori}}" name="kategori" class="form-control" />
                            </div>
                        </div>
                        @error('kategori')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Keterangan</label>
                               <textarea name="keterangan" id="" class="form-control">{{$kategori_buku->keterangan}}</textarea>
                            </div>
                        </div>
                        @error('kategori')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" style="margin-bottom:10px; margin-left:6px;" class="btn btn-primary">Simpan Data</button>
            </div>
          </form>
    </div>
    </div>
@endsection