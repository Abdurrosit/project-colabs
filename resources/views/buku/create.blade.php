@extends('layouts.master')

@section('judul')
Halaman Tambah Buku
@endsection
@section('content')
<div class="row">
    <div class="card">
        <form action="/buku" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="body">
            <h3 class="card-aside-title ">Tambah Data Buku</h3>
                <div class="row ">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Judul Buku</label>
                                <input type="text" name="judul" class="form-control" />
                            </div>
                        </div>
                        @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                        <label>Kategori</label>
                        <select name="kategori_id" class="form-control">
                            <option value="">--Pilih Kategori--</option>
                            @foreach ($kategori_buku as $item)
                                <option value="{{$item->id}}">{{$item->kategori}}</option>
                            @endforeach
                        </select>
                        </div>
                        @error('kategori')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Pengarang</label>
                                <input type="text" name="pengarang" class="form-control"/>
                            </div>
                        </div>
                        @error('pengarang')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Penerbit</label>
                                <input type="text" name="penerbit" class="form-control" />
                            </div>
                        </div>
                        @error('penerbit')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Tahun Terbit</label>
                                <input type="date" name="tahun" class="form-control" />
                            </div>
                        </div>
                        @error('Tahun')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Deskripsi</label>
                                <input type="text" name="deskripsi" class="form-control" />
                            </div>
                        </div>
                        @error('deskripsi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Jumlah Buku</label>
                                <input type="number" name="stock" class="form-control" />
                            </div>
                        </div>
                        @error('stock')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label>Letak Buku</label>
                                <select name="rak" class="form-control" id="">
                                    <option value=""selected>--Pilih Lemari--</option>
                                    <option value="Lemari 1">Lemari 1</option>
                                    <option value="Lemari 2">Lemari 2</option>
                                    <option value="Lemari 3">Lemari 3</option>
                                </select>
                            </div>
                        </div>
                        @error('rak')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nama User</label>
                                <input type="number" name="user_id" class="form-control">
                            </div>
                        </div>
                        @error('user_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" style="margin-bottom:10px; margin-left:6px;" class="btn btn-primary">Simpan Data</button>
            </div>
          </form>
    </div>
    </div>
@endsection