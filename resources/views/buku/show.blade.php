@extends('layouts.master')

@section('judul')
Detail Buku
@endsection
@section('content')
<div class="row">
    <div class="card">
        <form action="/buku" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="body">
            <h3 class="card-aside-title ">Detail Buku {{$buku->judul}}</h3>
                <div class="row ">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Judul Buku</label>
                                <input type="text" value="{{$buku->judul}}" class="form-control" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                        <label>Kategori</label>
                        <input type="text" value="{{$buku->kategori_buku->kategori}}" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Pengarang</label>
                                <input type="text" value="{{$buku->pengarang}}" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Penerbit</label>
                                <input type="text" value="{{$buku->penerbit}}" class="form-control" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Tahun Terbit</label>
                                <input type="date" value="{{$buku->tahun}}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Deskripsi</label>
                                <input type="number" value="{{$buku->deskripsi}}" class="form-control" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Jumlah Buku</label>
                                <input type="number" value="{{$buku->stock}}" class="form-control" readonly />
                            </div>
                        </div>
                        @error('stock')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label>Letak Buku</label>
                                <input type="text" value="{{$buku->rak}}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nama User</label>
                                <input type="number" name="user_id" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
               <a href="/buku" class="btn btn-danger">Kembali</a>
            </div>
          </form>
    </div>
</div>
@endsection