@extends('layouts.master')

@section('judul')
Halaman Edit Buku
@endsection
@section('content')
<div class="row">
    <div class="card">
        <form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="body">
            <h3 class="card-aside-title ">Edit Buku {{$buku->judul}}</h3>
                <div class="row ">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Judul Buku</label>
                                <input type="text" name="judul" value="{{$buku->judul}}" class="form-control" />
                            </div>
                        </div>
                        @error('judul')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                        <label>Kategori</label>
                        <select name="kategori_id" class="form-control">
                            @foreach ($kategori_buku as $item)
                            @if($item->id === $buku->kategori_id)
                                 <option value="{{$item->id}}" selected>{{$item->kategori}}</option>
                            @else
                                 <option value="{{$item->id}}">{{$item->kategori}}</option>
                            @endif
                            @endforeach
                        </select>
                        </div>
                        @error('kategori')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Pengarang</label>
                                <input type="text" name="pengarang" value="{{$buku->pengarang}}" class="form-control"/>
                            </div>
                        </div>
                        @error('pengarang')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Penerbit</label>
                                <input type="text" name="penerbit" value="{{$buku->penerbit}}" class="form-control" />
                            </div>
                        </div>
                        @error('penerbit')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Tahun Terbit</label>
                                <input type="date" name="tahun" value="{{$buku->tahun}}" class="form-control" />
                            </div>
                        </div>
                        @error('Tahun')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Deskripsi</label>
                                <input type="text" name="deskripsi" value="{{$buku->deskripsi}}" class="form-control" />
                            </div>
                        </div>
                        @error('deskripsi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                            <label for="">Jumlah Buku</label>
                                <input type="number" name="stock" value="{{$buku->stock}}" class="form-control" />
                            </div>
                        </div>
                        @error('stock')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label>Letak Buku</label>
                                <select name="rak" class="form-control" id="">
                                    @if ($buku->rak ="Lemari 1")
                                        <option value="Lemari 1" selected>Lemari 1</option>
                                        <option value="Lemari 2" >Lemari 2</option>
                                        <option value="Lemari 3" >Lemari 3</option>
                                    @elseif ($buku->rak ="Lemari 2")
                                        <option value="Lemari 1">Lemari 1</option>
                                        <option value="Lemari 2" selected>Lemari 2</option>
                                        <option value="Lemari 3">Lemari 3</option>
                                    @else
                                        <option value="Lemari 1">Lemari 1</option>
                                        <option value="Lemari 2">Lemari 2</option>
                                        <option value="Lemari 3" selected>Lemari 3</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        @error('rak')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label>Nama User</label>
                                <input type="number" value="{{$buku->user_id}}" name="user_id" class="form-control">
                            </div>
                        </div>
                        @error('user_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" style="margin-bottom:10px; margin-left:10px" class="btn btn-primary">Edit Data</button>
            </div>
          </form>
    </div>
    </div>
@endsection