@extends('layouts.master')

@section('judul')
Halaman Edit Peminjaman
@endsection


@section('content')
<form action="/peminjaman/{{$peminjaman->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama Anggota</label>
        <select name="anggota_id" class="form-control">
              <option value="">--pilih anggota--</option>
              @foreach ($anggota as $item)
                  <option value="{{$item->id}}">{{$item->anggota}}</option>
              @endforeach
        </select>
    </div>
    @error('anggota')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Judul Buku</label>
        <select name="buku_id" class="form-control">
              <option value="">--pilih buku--</option>
              @foreach ($buku as $item)
                  <option value="{{$item->id}}">{{$item->buku}}</option>
              @endforeach
        </select>
    </div>
    @error('buku')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Kode Peminjaman</label>
      <input type="text" name="kode_pinjam" class="form-control">
    </div>
    @error('kode_pinjam')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tanggal Peminjaman</label>
      <input type="date" name="tgl_pinjam" class="form-control">
    </div>
    @error('tgl_pinjam')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tanggal Pengembalian</label>
        <input type="date" name="tgl_kembali" class="form-control">
    </div>
    @error('tgl_kembali')
          <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Status Peminjaman</label>
        <input type="text" name="status" class="form-control">
    </div>
    @error('status')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Denda</label>
        <input type="text" name="denda" class="form-control">
    </div>
    @error('denda')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    @error('buku_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>   

  </form>

@endsection