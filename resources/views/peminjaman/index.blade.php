@extends('layouts.master')

@section('judul')
Data Transaksi Peminjaman
@endsection

@section('content')
    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Table Data Transaksi
                            </h2>
                        </div>
                        <div class="body">
                        <a href="/peminjaman/create" style="margin-bottom:7px" class="btn btn-primary my-2">Tambah Transaksi</a>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <td>Kategori</td>
                                    <td>keterangan</td>
                                    <td>aksi</td>
                                </thead>
                                    <tbody>
                                    @forelse($peminjaman as $item)
                                    <tr>
                                        <td>{{$item->kode_pinjam}}</td> 
                                        <td>{{$item->tgl_pinjam}}</td>
                                        <td>
                                            <form action="/kategori_buku/{{$item->id}}" method="POST">
                                                @csrf
                                                @method ('DELETE')
                                                <a href="/kategori_buku/{{$item->id}}" class="btn btn-sm btn-success">detail</a>
                                                <a href="/kategori_buku/{{$item->id}}/edit" class="btn btn-sm btn-info">Edit</a>
                                                <input type="submit" class="btn btn-danger" value="Hapus">
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <h4>Data Buku Belum Ada</h4>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  
@endsection