@extends('layouts.master')

@section('judul')
Halaman Transaksi Peminjaman
@endsection
@section('content')
<div class="row">
    <div class="card">
        <form action="/kategori_buku" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="body">
            <h3 class="card-aside-title ">Tambah Transaksi</h3>
                <div class="row ">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Kode Pinjam</label>
                                <input type="text" name="kode_pinjam" class="form-control" value="{{$kode}}" readonly/>
                            </div>
                        </div>
                        @error('kode_pinjam')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                        <label>Nama Anggota</label>
                        <select name="anggota_id" class="form-control">
                            <option value="">--PILIH NAMA ANGGOTA--</option>
                            @foreach ($anggota as $item)
                                <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                        </select>
                        </div>
                        @error('anggota_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                        <label>Nama Buku</label>
                        <select name="buku_id" class="form-control">
                            <option value="">--PILIH NAMA BUKU--</option>
                            @foreach ($buku as $item)
                                <option value="{{$item->id}}">{{$item->judul}}</option>
                            @endforeach
                        </select>
                        </div>
                        @error('buku_id')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Tanggal Pinjam</label>
                            <input type="date" name="tgl_pinjam" class="form-control">
                            </div>
                        </div>
                        @error('tgl_pinjam')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Tanggal Kembali</label>
                            <input type="date" name="tgl_kembali" class="form-control">
                            </div>
                        </div>
                        @error('tgl_kembali')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Status</label>
                            <input type="text" name="status" class="form-control" value="Pinjam" readonly>
                            </div>
                        </div>
                        @error('status')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Denda</label>
                            <input type="number" name="denda" class="form-control">
                            </div>
                        </div>
                        @error('denda')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" style="margin-bottom:10px; margin-left:6px;" class="btn btn-primary">Simpan Data</button>
            </div>
          </form>
    </div>
    </div>
@endsection