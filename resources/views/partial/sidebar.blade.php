<aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="admin/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @auth
                        {{ Auth::user()->name }}
                        @endauth
                    </div>
                    <div class="email">
                        @auth
                        {{ Auth::user()->email }}
                        @endauth
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN MENU</li>
                    <li>
                        <a href="/home">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="/peminjaman">
                            <i class="material-icons">queue_play_next</i>
                            <span>Transaksi</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">view_list</i>
                            <span>Master Data</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="/anggota">
                                    <i class="material-icons">people</i>
                                    <span>Anggota</span>
                                </a>
                            </li>
                            <li>
                                <a href="/buku">
                                    <i class="material-icons">call_to_action</i>
                                    <span>Buku</span>
                                </a>
                            </li>
                            <li>
                                <a href="/kategori_buku">
                                    <i class="material-icons">account_box</i>
                                    <span>Kategori Buku</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                    <a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          <i class="material-icons">power_settings_new</i>
                          <span>Logout</span>                    
                    </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    
            <!-- #Menu -->
            <!-- Footer -->
            <!-- #Footer -->
        </aside>