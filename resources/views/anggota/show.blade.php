@extends('layouts.master')

@section('judul')
Halaman Detail Anggota
@endsection
@section('content')
<div class="row">
    <div class="card">
        <form action="/anggota/{{$anggota->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method ('PUT')
            <div class="body">
            <h3 class="card-aside-title ">Detail {{$anggota->nama}}</h3>
                <div class="row ">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Nomor Anggota</label>
                                <input type="text" name="no_anggota" value="{{$anggota->no_anggota}}" class="form-control" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Nama Anggota</label>
                               <input type="text" name="nama" value="{{$anggota->nama}}"class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Tanggal Lahir</label>
                               <input type="date" name="tgl_lahir" value="{{$anggota->tgl_lahir}}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label>Jenis Kelamin</label>
                               <input type="text" value="{{$anggota->jk}}" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label for=""> Alamat</label>
                               <textarea name="alamat" class="form-control" readonly>{{$anggota->alamat}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">No Telpon</label>
                               <input type="number" name="telpon" value="{{$anggota->telpon}}" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
               <a href="/anggota" style="margin-bottom:10px; margin-left:6px;" class="btn btn-danger">Kembali</a>
            </div>
          </form>
    </div>
    </div>
@endsection