@extends('layouts.master')

@section('judul')
Tambah Kategori Buku
@endsection

@section('content')
    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Data Anggota
                            </h2>
                        </div>
                        <div class="body">
                        <a href="/anggota/create" style="margin-bottom:7px" class="btn btn-success my-2">Tambah Anggota</a>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <td>No Anggota</td>
                                    <td>Nama Anggota</td>
                                    <td>Tgl Lahir</td>
                                    <td>Jenis Kelamin</td>
                                    <td>Alamat</td>
                                    <td>No Telpon</td>
                                    <td>aksi</td>
                                </thead>
                                    <tbody>
                                    @forelse($anggota as $item)
                                    <tr>
                                        <td>{{$item->no_anggota}}</td> 
                                        <td>{{$item->nama}}</td>
                                        <td>{{$item->tgl_lahir}}</td>
                                        <td>{{$item->jk}}</td>
                                        <td>{{$item->alamat}}</td>
                                        <td>{{$item->telpon}}</td>
                                        <td>
                                            <form action="/anggota/{{$item->id}}" method="POST">
                                                @csrf
                                                @method ('DELETE')
                                                <a href="/anggota/{{$item->id}}" class="btn btn-sm btn-success">Detail</a>
                                                <a href="/anggota/{{$item->id}}/edit" class="btn btn-sm btn-info">Edit</a>
                                                <input type="submit" class="btn btn-danger" value="Hapus">
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <h4>Data Buku Belum Ada</h4>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  
@endsection