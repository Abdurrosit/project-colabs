@extends('layouts.master')

@section('judul')
Halaman Tambah Anggota
@endsection
@section('content')
<div class="row">
    <div class="card">
        <form action="/anggota/{{$anggota->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method ('PUT')
            <div class="body">
            <h3 class="card-aside-title ">Edit Data  Anggota</h3>
                <div class="row ">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Nomor Anggota</label>
                                <input type="text" name="no_anggota" value="{{$anggota->no_anggota}}" class="form-control" readonly/>
                            </div>
                        </div>
                        @error('no_anggota')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Nama Anggota</label>
                               <input type="text" name="nama" value="{{$anggota->nama}}"class="form-control">
                            </div>
                        </div>
                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">Tanggal Lahir</label>
                               <input type="date" name="tgl_lahir" value="{{$anggota->tgl_lahir}}" class="form-control">
                            </div>
                        </div>
                        @error('tgl_lahir')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label>Jenis Kelamin</label>
                               <select name="jk" id="" class="form-control">
                                @if ($anggota->jk =="Laki-laki")
                                    <option value="Laki-laki" selected>Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                @else
                                    <option value="Laki-laki">Laki-laki</option>
                                   <option value="Perempuan" selected>Perempuan</option>
                                @endif
                               </select>
                            </div>
                        </div>
                        @error('jk')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for=""> Alamat</label>
                               <textarea name="alamat" class="form-control">{{$anggota->alamat}}</textarea>
                            </div>
                        </div>
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">No Telpon</label>
                               <input type="number" name="telpon" value="{{$anggota->telpon}}" class="form-control">
                            </div>
                        </div>
                        @error('telpon')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" style="margin-bottom:10px; margin-left:6px;" class="btn btn-primary">Update Data</button>
                <a href="/anggota" style="margin-bottom:10px; margin-left:6px;" class="btn btn-danger">Batal</a>
            </div>
          </form>
    </div>
    </div>
@endsection