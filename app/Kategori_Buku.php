<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori_Buku extends Model
{
    protected $table = 'kategori_buku';
    protected $fillable = ['kategori','keterangan'];

    public function buku()
    {
        return $this->hasMany('App\Buku');
    }
}
