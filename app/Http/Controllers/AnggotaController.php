<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anggota;
use RealRashid\SweetAlert\Facades\Alert;

use File;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = Anggota::all();

        return view('anggota.index', compact('anggota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getRow = anggota::orderBy('id', 'DESC')->get();
        $rowCount = $getRow->count();
        
        $lastId = $getRow->first();

        $kode = "APK00001";
        
        if ($rowCount > 0) {
            if ($lastId->id < 9) {
                    $kode = "APK0000".''.($lastId->id + 1);
            } else if ($lastId->id < 99) {
                    $kode = "APK000".''.($lastId->id + 1);
            } else if ($lastId->id < 999) {
                    $kode = "APK00".''.($lastId->id + 1);
            } else if ($lastId->id < 9999) {
                    $kode = "APK0".''.($lastId->id + 1);
            } else {
                    $kode = "APK".''.($lastId->id + 1);
            }
        }

        return view('anggota.create', compact('kode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_anggota' => 'required',
            'tgl_lahir' => 'required',
            'jk' => 'required',
            'alamat' => 'required',
            'telpon' => 'required',
            'nama' => 'required',
        ]);

        $anggota = new Anggota;

        $anggota->no_anggota = $request->no_anggota;
        $anggota->tgl_lahir = $request->tgl_lahir;
        $anggota->jk = $request->jk;
        $anggota->alamat = $request->alamat;
        $anggota->telpon = $request->telpon;
        $anggota->nama = $request->nama;

        $anggota->save();

        Alert::success('Berhasil', 'Berhasil Tambah Anggota');
        return redirect('/anggota');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggota = Anggota::findOrFail($id);
        return view('anggota.show', compact('anggota'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota = Anggota::findOrFail($id);
        return view('anggota.edit', compact('anggota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'no_anggota' => 'required',
            'tgl_lahir' => 'required',
            'jk' => 'required',
            'alamat' => 'required',
            'telpon' => 'required',
        ]);

        $anggota = Anggota::find($id);

        $anggota->nama = $request->nama;
        $anggota->no_anggota = $request->no_anggota;
        $anggota->tgl_lahir = $request->tgl_lahir;
        $anggota->jk = $request->jk;
        $anggota->alamat = $request->alamat;
        $anggota->telpon = $request->telpon;

        $anggota->update();

        Alert::success('Berhasil', 'Berhasil Edit Data');
        return redirect('/anggota');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anggota = Anggota::find($id);
        $anggota->delete();

        Alert::success('Berhasil', 'Berhasil Hapus Data');
        return redirect('/anggota');
    }
}
