<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori_Buku;
use RealRashid\SweetAlert\Facades\Alert;

class KategoribukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori_buku = Kategori_Buku::all();

        return view('kategori_buku.index', compact('kategori_buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori_buku.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kategori' => 'required',
            'keterangan' => 'required',
        ]);

        $kategori_buku = new Kategori_Buku;

        $kategori_buku->kategori = $request->kategori;
        $kategori_buku->keterangan = $request->keterangan;

        $kategori_buku->save();
        Alert::success('Berhasil', 'Berhasil Tambah Data');
        return redirect('/kategori_buku');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori_buku = Kategori_Buku::findOrFail($id);
        return view('kategori_buku.show', compact('kategori_buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori_buku = Kategori_Buku::findOrFail($id);
        return view('kategori_buku.edit', compact('kategori_buku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kategori' => 'required',
            'keterangan' => 'required',
        ]);

        $kategori_buku = Kategori_Buku::find($id);

        $kategori_buku->kategori = $request->kategori;
        $kategori_buku->keterangan = $request->keterangan;

        $kategori_buku->update();
        Alert::success('Berhasil', 'Berhasil Edit Data');
        return redirect('/kategori_buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori_buku = Kategori_Buku::find($id);
        $kategori_buku->delete();

        Alert::success('Berhasil', 'Berhasil Hapus Data');
        return redirect('/kategori_buku');
    }
}
