<?php

namespace App\Http\Controllers;

use App\Peminjaman;
use App\Anggota;
use App\Buku;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $peminjaman = Peminjaman::get();
        $anggota   = Anggota::get();
        $buku      = Buku::get();
        
        return view('halaman.dashboard', compact('peminjaman', 'anggota', 'buku'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
}
