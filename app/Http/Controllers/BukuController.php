<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Kategori_Buku;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori_buku = Kategori_Buku::all();
        return view('buku.create', compact('kategori_buku'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'stock' => 'required',
            'rak' => 'required',
            'user_id' => 'required',
            'kategori_id' => 'required',
        ]);

        $buku = new Buku;

        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->tahun = $request->tahun;
        $buku->deskripsi = $request->deskripsi;
        $buku->stock = $request->stock;
        $buku->rak = $request->rak;
        $buku->user_id = $request->user_id;
        $buku->kategori_id = $request->kategori_id;

        $buku->save();
        Alert::success('Berhasil', 'Berhasil Tambah Data');
        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori_buku = Kategori_Buku::all();
        $buku = Buku::findOrFail($id);
        return view('buku.edit', compact('buku','kategori_buku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
            'deskripsi' => 'required',
            'stock' => 'required',
            'rak' => 'required',
            'user_id' => 'required',
            'kategori_id' => 'required',
        ]);

        $buku = Buku::find($id);

        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->tahun = $request->tahun;
        $buku->deskripsi = $request->deskripsi;
        $buku->stock = $request->stock;
        $buku->rak = $request->rak;
        $buku->user_id = $request->user_id;
        $buku->kategori_id = $request->kategori_id;

        $buku->update();
        Alert::success('Berhasil', 'Berhasil Edit Data');
        return redirect('/buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);
        $buku->delete();

        Alert::success('Berhasil', 'Berhasil Hapus Data');
        return redirect('/buku');
    }
}
