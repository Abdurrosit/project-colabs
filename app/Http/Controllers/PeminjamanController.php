<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Anggota;
use App\Peminjaman;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peminjaman = Peminjaman::all();
        return view('peminjaman.index', compact('peminjaman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getRow = Peminjaman::orderBy('id', 'DESC')->get();
        $rowCount = $getRow->count();
        
        $lastId = $getRow->first();

        $kode = "TRX00001";
        
        if ($rowCount > 0) {
            if ($lastId->id < 9) {
                    $kode = "TRX0000".''.($lastId->id + 1);
            } else if ($lastId->id < 99) {
                    $kode = "TRX000".''.($lastId->id + 1);
            } else if ($lastId->id < 999) {
                    $kode = "TRX00".''.($lastId->id + 1);
            } else if ($lastId->id < 9999) {
                    $kode = "TRX0".''.($lastId->id + 1);
            } else {
                    $kode = "TRX".''.($lastId->id + 1);
            }
        }

        $buku = Buku::where('stock', '>', 0)->get();
        $anggota = Anggota::get();
        return view('peminjaman.create', compact('buku','kode', 'anggota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_pinjam' => 'required',
            'anggota_id' => 'required',
            'buku_id' => 'required',
            'tgl_pinjam' => 'required',
            'tgl_kembali' => 'required',
            'status' => 'required',
            'denda' => 'required',
            
        ]);

        $peminjaman = new Peminjaman;

        $peminjaman->kode_pinjam = $request->kode_pinjam;
        $peminjaman->anggota_id = $request->anggota_id;
        $peminjaman->buku_id = $request->buku_id;
        $peminjaman->tgl_pinjam = $request->tgl_pinjam;
        $peminjaman->tgl_kembali = $request->tgl_kembali;
        $peminjaman->status = $request->status;
        $peminjaman->denda = $request->denda;
        
        $peminjaman->save();
        return redirect('/peminjaman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peminjaman = Peminjaman::findOrFail($id);
        return view('peminjaman.show', compact('peminjaman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penjaminan = Penjaminan::findOrFail($id);
        return view('penjaminan.edit', compact('penjaminan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'anggota_id' => 'required',
            'buku_id' => 'required',
            'kode_pinjam' => 'required',
            'tgl_pinjam' => 'required',
            'tgl_kembali' => 'required',
            'status' => 'required',
            'denda' => 'required',
            
        ]);

        $peminjaman->anggota_id = $request->anggota_id;
        $peminjaman->buku_id = $request->buku_id;
        $peminjaman->kode_pinjam = $request->kode_pinjam;
        $peminjaman->tgl_pinjam = $request->tgl_pinjam;
        $peminjaman->tgl_kembali = $request->tgl_kembali;
        $peminjaman->status = $request->status;
        $peminjaman->denda = $request->denda;

        $peminjaman->update();

        return redirect('/peminjaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peminjaman = Peminjaman::find($id);
        $peminjaman->delete();

        return redirect('/peminjaman');
    }
}
