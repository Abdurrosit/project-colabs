<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $fillable = ['judul','pengarang','penerbit','tahun','deskripsi','stock','rak','user_id','kategori_id'];

    public function kategori_buku()
    {
        return $this->belongsTo('App\Kategori_Buku','kategori_id');
    }
}
