<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $table = 'anggota';
    protected $fillable = ['nama','no_anggota','tgl_lahir','jk','alamat','telpon'];
}
