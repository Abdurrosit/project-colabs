<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
    protected $fillable = ['kode_pinjam', 'tgl_pinjam', 'tgl_kembali', 'status', 'denda', 'anggota_id', 'buku_id'];
}
